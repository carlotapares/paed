package structure;

import base.TableDataStructure;
import base.TableRow;
import base.TableRowRestriction;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Carlota & Dani
 */
public class TwoThreeTree extends TableDataStructure{
    private Node root;

    private Node lNode;
    private Node mNode;
    private TableRow key;
    private TableRow succ;
    private boolean success;

    // If a split operation occurs, we'll have to turn a 3-node into a
    // 4-node. (placeholder)
    private TableRow[] keys = new TableRow[3];
    private Node[] nodes = new Node[4];

    @Override
    public List<TableRow> getHistoric(Object indexValue) {
        if (root == null) return null;
        else {
            TableRow aux = new TableRow();
            aux.setColumn(index,indexValue);
            return findRec(root,aux);
        }
    }

    /**
     * Method that returns true if the tree has 0 nodes
     * @return root == null
     */
    public boolean isEmpty() {
        return root == null;
    }

    public Node getRoot() {
        return root;
    }


    /**
     * Recursive method that finds the node where the key must be placed and places it there considering
     * the insertion rules
     * @param node node to consider
     * @return return true if the root must be split up into a new node
     */
    private boolean insertRec(Node node) {

        // This is an internal node. We need to reach
        // the appropriate child using recursion.

        if (node.getLeft() != null) {
            // If the key to insert is smaller than the smaller key of the actual node we
            // start recursion through the first child of the node
            if (key.compareTo(index,node.getK1()) < 0) {
                // If there hasn't been a split, we return false
                if (!insertRec(node.getLeft())) {
                    return false;
                }
                // If there has been a split we ought to consider different cases:

                // Case node has only one key (it is a 2-structure.Node)
                if (node.getK2() == null) {

                    // Example : inserting 35
                    //                     ______
                    //                      48 -   (node)
                    //                     ------
                    //   (node.left)   45 47   50 52 (node.middle)
                    //                 -----   -----
                    //                     _______
                    //                     45   48 (node)  (45 --> key)
                    //                     -------
                    //                35 -  47 -  50 52 (node.middle --> node.right)
                    //                ----  ----  -----
                    //   (35 --> lNode)  (47 --> mNode)
                    //

                    node.setK2(node.getK1());
                    node.setK1(key);
                    node.setRight(node.getMiddle());
                    node.setMiddle(mNode);
                    node.setLeft(lNode);
                    return false;

                }
                // Otherwise we need to convert this 3-node into a 4-node.
                else {

                    // Example : inserting 19
                    //                     ______
                    //                      23 30   (node)
                    //                     ------
                    //                20 21  24 -  31 -
                    //                -----  ----  ----
                    //                    _______
                    //                    20 23 30   (node)
                    //                    -------
                    //              19 - 21 - 24 -  31 -
                    //              ---- ---- ----  ----
                    //

                    keys[0] = key;
                    keys[1] = node.getK1();
                    keys[2] = node.getK2();
                    nodes[0] = lNode;
                    nodes[1] = mNode;
                    nodes[2] = node.getMiddle();
                    nodes[3] = node.getRight();
                }
            }
            // Case it is a 3-structure.Node
            else if (node.getK2() != null) {
                // If there is k2, we consider recursion to the middle node or to the right node

                // Start recursion to the second child of the node
                if (key.compareTo(index,node.getK2()) < 0) {
                    // If there hasn't been a split, we return false
                    if (!insertRec(node.getMiddle())) {
                        return false;
                    }

                    // If there has been a split we do the following
                    // Example : inserting 27
                    //                     ______
                    //                      17 33   (node)
                    //                     ------
                    //                12 -  22 25  36 38
                    //                ----  -----  ----
                    //                    _______
                    //                    17 25 33   (node)
                    //                    -------
                    //              12 - 22 - 27 -  36 38
                    //              ---- ---- ----  ----
                    //

                    keys[0] = node.getK1();
                    keys[1] = key;
                    keys[2] = node.getK2();
                    nodes[0] = node.getLeft();
                    nodes[1] = lNode;
                    nodes[2] = mNode;
                    nodes[3] = node.getRight();

                }
                // Start recursion to the third child of the node
                else if (key.compareTo(index,node.getK2()) > 0) {
                    // If there hasn't been a split, we return false
                    if (!insertRec(node.getRight())) {
                        return false;
                    }

                    // If there has been a split we do the following
                    // Example : inserting 40
                    //                     ______
                    //                      17 33   (node)
                    //                     ------
                    //                12 -  22 25  36 38
                    //                ----  -----  ----
                    //                    _______
                    //                    17 33 38  (node)
                    //                    -------
                    //              12 - 22 25 36 - 40 -
                    //              ---- ----- ---- ----
                    //

                    keys[0] = node.getK1();
                    keys[1] = node.getK2();
                    keys[2] = key;
                    nodes[0] = node.getLeft();
                    nodes[1] = node.getMiddle();
                    nodes[2] = lNode;
                    nodes[3] = mNode;

                }

            }
            // Case it is a 2-structure.Node
            // Start recursion to the middle child of the node
            else if (key.compareTo(index,node.getK1()) > 0) {
                // If there hasn't been a split, we return false
                if (!insertRec(node.getMiddle())) {
                    return false;
                }
                // If there has been a split we do the following
                // Example : inserting 60
                //                     ______
                //                      48 -   (node)
                //                     ------
                //   (node.left)   45 47   50 52 (node.middle)
                //                 -----   -----
                //                     _______
                //                     48  52 (node)  (52 --> key)
                //                     -------
                //                45 47 50 -  60 - (node.middle --> node.right)
                //                ----  ----  -----
                //   (50 --> lNode)  (60 --> mNode)
                //
                node.setK2(key);
                node.setMiddle(lNode);
                node.setRight(mNode);
                return false;
            }
            else {
                success = false;
                return false;
            }
        }
        // We hit a leaf node
        else {
            // This leaf only contains one key.  No split is required, we just
            // need to insert the new key into the correct place.
            if (node.getK2() == null){
                if (key.compareTo(index,node.getK1()) > 0) {
                    node.setK2(key);
                } else if (key.compareTo(index,node.getK1()) < 0) {
                    node.setK2(node.getK1());
                    node.setK1(key);
                } else {
                    success = false;
                }
                return false;
            }
            // If the leaf contains 2 keys, we consider the following cases:

            // Case 'key' is smaller than K1:
            if (key.compareTo(index,node.getK1()) < 0) {

                // Example: inserting 12
                //                     ______
                //                      23 40   (node)
                //                     ------
                //                     ________
                //                     12 23 40
                //                     --------

                keys[0] = key;
                keys[1] = node.getK1();
                keys[2] = node.getK2();
            }
            // Case 'key' is greater than K1 and smaller than K2
            else if (key.compareTo(index,node.getK2()) < 0) {

                // Example: inserting 30
                //                     ______
                //                      23 40   (node)
                //                     ------
                //                     ________
                //                     23 30 40
                //                     --------

                keys[0] = node.getK1();
                keys[1] = key;
                keys[2] = node.getK2();
            }
            else if (key.compareTo(index,node.getK2()) == 0) {
                success = false;
                return false;
            }
            // Case 'key' is greater than K2
            else {

                // Example: inserting 43
                //                     ______
                //                      23 40   (node)
                //                     ------
                //                     ________
                //                     23 40 43
                //                     --------

                keys[0] = node.getK1();
                keys[1] = node.getK2();
                keys[2] = key;
            }
            //Because 'node' is a leaf, no childs exist.
            nodes [0] = null;
            nodes [1] = null;
            nodes [2] = null;
            nodes [3] = null;
        }

        // If we created a 4-structure.Node we need to split up the node. We take as an example the following:
        // If there has been a split we do the following
        // Example : inserting 40
        //                     ______
        //                      17 33   (node)
        //                     ------
        //                12 -  22 25  36 38
        //                ----  -----  ----
        //                    _______
        //                    17 33 38  (node)
        //                    -------
        //              12 - 22 25 36 - 40 -
        //              ---- ----- ---- ----
        //
        // Here we see that we have: keys = {17,33,38} and nodes = {(12,-), (22,25), (36,-), (40,-)}
        // So now, our node becomes:
        //
        //                     ______
        //                      17 -   (node)
        //                     ------
        //                12 -  22 25
        //                ----  -----
        //
        // And our auxiliary node becomes:
        //                     ______
        //                      38 -   (aux)
        //                     ------
        //                36 -  40 -
        //                ----  -----
        //
        // 'key' becomes 33
        // and we assign node and aux to lNode and mNode

        node.setK1(keys[0]);
        node.setK2(null);
        node.setLeft(nodes[0]);
        node.setMiddle(nodes[1]);
        node.setRight(null);

        Node aux = new Node(keys[2]);
        aux.setLeft(nodes[2]);
        aux.setMiddle(nodes[3]);

        // Return the info about these two sibling nodes so the caller can
        // handle the split at its level.

        this.key  = keys[1];
        lNode = node;
        mNode = aux;
        return true;

    }

    /**
     * Method that finds the smallest key that comes immediately after this node
     * @param node
     * @return returns true if the node where we have removed the key is now a 1-structure.Node
     */
    private boolean inOrderSuccessor(Node node){
        while (node.getLeft() != null) {
            node = node.getLeft();
        }

        this.succ = node.getK1();
        node.setK1(node.getK2());
        node.setK2(null);

        return (node.getK1() == null);
    }

    /**
     * Method that fixes the disorder caused by applying inOrderSuccessor to the tree
     * @param node
     * @return returns true if we must fix the left child
     */
    private boolean fixInOrderSuccessor(Node node){

        // If it is a leaf no work needs to be done
        if (node.getLeft() == null) {
            return true;
        }

        // If it has a child we merge!
        if (fixInOrderSuccessor(node.getLeft())) {
            //
            // Example:
            //              _____
            //              50 52
            //              -----
            //          _____ ____ ____
            //          - -  51 - 55 57
            //          ----- ---- ----
            //
            return fixLeft(node);
        }
        return false;
    }

    /**
     * Method that fixes the disorder caused by deleting a key from the left child
     * @param node
     * @return returns true if more work needs to be done one the upper layer
     */
    private boolean fixLeft (Node node){
        Node lchild = node.getLeft();
        Node mchild = node.getMiddle();
        // If the middle child is a 3-structure.Node we steal a key from it
        if (mchild.getK2() != null) {
            //
            // Example:
            //              _____
            //              50 53
            //              -----
            //          _____ ____ ____
            //          - -  51 52 55 57
            //          ----- ---- ----
            //
            //              _____
            //              51 53
            //              -----
            //          _____ ____ ____
            //          50 -  52 - 55 57
            //          ----- ---- ----
            //
            //

            lchild.setK1(node.getK1());
            node.setK1(mchild.getK1());
            mchild.setK1(mchild.getK2());
            mchild.setK2(null);


            lchild.setMiddle(mchild.getLeft());
            mchild.setLeft(mchild.getMiddle());
            mchild.setMiddle(mchild.getRight());
            mchild.setRight(null);
            return false;
        }

        //If the middle child is a 2-node

        //
        // Example:
        //              _____
        //              50 53
        //              -----
        //          _____ ____ ____
        //          - -  51 - 55 57
        //          ----- ---- ----
        //
        //              _____
        //              53 -
        //              -----
        //          _____ ____  ____
        //          51 52 55 57 - -
        //          ----- ----  ----
        //
        //
        lchild.setK1(node.getK1());
        lchild.setK2(mchild.getK1());
        mchild.setK1(null);

        lchild.setMiddle(mchild.getLeft());
        lchild.setRight(mchild.getMiddle());

        mchild.setLeft(null);
        mchild.setMiddle(null);
        mchild.setRight(null);

        node.setK1(node.getK2());
        node.setK2(null);
        node.setMiddle(node.getRight());
        node.setRight(null);

        return (node.getK1() == null);
    }

    /**
     * Method that fixes the disorder caused by deleting a key from the middle child
     * @param node
     * @return
     */
    private boolean fixMiddle(Node node){

        Node lchild = node.getLeft();
        Node mchild = node.getMiddle();
        Node rchild = node.getRight();

        // If the node has a right child which is 3-structure.Node we steal a key from it
        if ((rchild != null) && (rchild.getK2() != null)) {
            //
            // Example:
            //              _____
            //              50 52
            //              -----
            //          _____ ____ ____     lchild = (45,47), mchild = (-,-),  rchild = (55,57)
            //          45 47 - -  55 57
            //          ----- ---- ----
            //              _____
            //              50 55
            //              -----
            //          _____ ____ ____
            //          45 47 52 -  57 -
            //          ----- ---- ----
            //

            mchild.setK1(node.getK2());
            node.setK2(rchild.getK1());
            rchild.setK1(rchild.getK2());
            rchild.setK2(null);

            mchild.setMiddle(rchild.getLeft());
            rchild.setLeft(rchild.getMiddle());
            rchild.setMiddle(rchild.getRight());
            rchild.setRight(null);


            return false;
        }
        // If the node has a left child which is 3-structure.Node we steal a key from it
        if (lchild.getK2() != null) {
            //
            // Example:
            //              _____
            //              50 52
            //              -----
            //          _____ ____ ____     lchild = (45,47), mchild = (-,-),  rchild = (55,57)
            //          45 47 - -  55 57
            //          ----- ---- ----
            //              _____
            //              47 52
            //              -----
            //          _____ ____ ____
            //          45 -  50 - 55 57
            //          ----- ---- ----
            //
            mchild.setK1(node.getK1());
            node.setK1(lchild.getK2());
            lchild.setK2(null);


            mchild.setMiddle(mchild.getLeft());
            mchild.setLeft(lchild.getRight());
            lchild.setRight(null);

            return false;
        }
        //If the node is a 3-node
        if (node.getK2() != null) {
            //
            // Example:
            //              _____
            //              50 52
            //              -----
            //          _____ ____ ____     lchild = (45,-), mchild = (-,-),  rchild = (55,-)
            //          45 -  - -  55 -
            //          ----- ---- ----
            //              _____
            //              50 -
            //              -----
            //          _____ ____
            //          45 -  52 55
            //          ----- ----
            //
            mchild.setK1(node.getK2());
            mchild.setK2(rchild.getK1());
            node.setK2(null);
            rchild.setK1(null);


            mchild.setMiddle(rchild.getLeft());
            mchild.setRight(rchild.getMiddle());
            rchild.setLeft(null);
            rchild.setMiddle(null);
            node.setRight(null);
            return false;
        }
        //If the node is a 2-structure.Node
        //
        // Example:
        //              _____
        //              50  -
        //              -----
        //          _____ ____ ____     lchild = (45,-), mchild = (-,-),  rchild = (55,-)
        //          45 -  - -  55 -
        //          ----- ---- ----
        //              _____
        //              -  -
        //              -----
        //          _____ ____ ____
        //          45 50 - -  55 -
        //          ----- ---- ----
        lchild.setK2(node.getK1());
        node.setK1(null);

        lchild.setRight(mchild.getLeft());
        mchild.setLeft(null);
        node.setMiddle(null);

        // We return true because we have to merge with the upper level
        return true;
    }

    /**
     * Method that fixes the disorder caused by deleting a key from the right child
     * @param node
     */
    private void fixRight (Node node){

        Node mchild = node.getMiddle();
        Node rchild = node.getRight();

        // If the middle child is a 3 -node we steal a key from it
        if (mchild.getK2() != null) {
            //
            // Example:
            //              _____
            //              48 55
            //              -----
            //          _____ ____  ____
            //          45 47 50 51 - -
            //          ----- ----  ----
            //
            //              _____
            //              48 51
            //              -----
            //          _____ ____  ____
            //          45 47 50 -  55 -
            //          ----- ----  ----
            //
            rchild.setK1(node.getK2());
            node.setK2(mchild.getK2());
            mchild.setK2(null);

            rchild.setMiddle(rchild.getLeft());
            rchild.setLeft(rchild.getRight());
            mchild.setRight(null);
        }
        // If the middle node is a 2_node
        else {
            //
            // Example:
            //              _____
            //              48 55
            //              -----
            //          _____ ____  ____
            //          45 47 50 -  - -
            //          ----- ----  ----
            //
            //              _____
            //              48 -
            //              -----
            //          _____ ____  ____
            //          45 47 50 55  - -
            //          ----- ----  ----
            //
            mchild.setK2(node.getK2());
            node.setK2(null);

            mchild.setRight(rchild.getLeft());
            rchild.setLeft(null);
            node.setRight(null);
        }
    }



    /**
     * Recursive method that searches for the key and if it is found, then it removes it
     * @param node
     * @return returns true if the root must be merged
     */
    private boolean deleteRec (Node node){

        // Case it is an internal node
        if (node.getLeft() != null) {
            // Start recursion to the first child of the node
            if (key.compareTo(index,node.getK1()) < 0) {
                // If the recursive call returns true, the node needs to be merged.
                if (deleteRec(node.getLeft())) {
                    return fixLeft(node);
                }
                else {
                    success = false;
                    return false;

                }
            }
            // We found the key to eliminar in the left most child of 'node'
            else if (key.compareTo(index,node.getK1()) == 0) {
                // If inOrderSuccessor() returns true, the node that
                // contained the successor key turned into a 1-node,
                // we need to merge
                //
                // Example: deleting 48
                //
                //              _____
                //              48 52
                //              -----
                //          _____ ____ ____
                //          45 47 50 - 55 -
                //          ----- ---- ----
                //
                // After we apply inOrderSuccessor()
                //
                //              _____
                //              48 52
                //              -----               succ = 50
                //          _____ ____ ____
                //          45 47 - -  55 -
                //          ----- ---- ----
                //
                if (inOrderSuccessor(node.getMiddle())) {
                    node.setK1(succ);
                    //              _____
                    //              50 52
                    //              -----
                    //          _____ ____ ____
                    //          45 47 - -  55 -
                    //          ----- ---- ----
                    //

                    // fixInOrderSuccessor returns true due to middle node is a 1-structure.Node leaf
                    if (fixInOrderSuccessor(node.getMiddle())) {
                        //we apply merge
                        //              _____
                        //              47 52
                        //              -----
                        //          _____ ____ ____
                        //          45 - 50 -  55 -
                        //          ----- ---- ----
                        return fixMiddle(node);
                    }
                }
                // If inOrderSuccessor returns false it means the middle node is a 2-structure.Node; we apply the following:
                // Example: deleting 48
                //
                //              _____
                //              48 52
                //              -----
                //          _____ ____  ____
                //          45 47 50 51 55 -
                //          ----- ----  ----
                //
                // After we apply inOrderSuccessor()
                //
                //              _____
                //              48 52
                //              -----               succ = 50
                //          _____ ____ ____
                //          45 47 51 -  55 -
                //          ----- ---- ----
                //

                else {
                    //              _____
                    //              50 52
                    //              -----               
                    //          _____ ____ ____
                    //          45 47 51 -  55 -
                    //          ----- ---- ----
                    //
                    node.setK1(succ);
                }
            }

            // If the node is a 3 -node
            else if (node.getK2() != null) {
                // If the key is greater than K1 and smaller than K2 we start recursion to the middle child of the node
                if (key.compareTo(index,node.getK2()) < 0) {
                    // If the recursive call returns true, the node needs to be merged.
                    if (deleteRec(node.getMiddle())) {
                        // Example: deleting 50
                        //
                        //              _____
                        //              48 52
                        //              -----
                        //          _____ ____  ____
                        //          45 47 50 - 55 -
                        //          ----- ----  ----
                        //
                        // After applying deleteRec to the leaf node (50,-)
                        //
                        //              _____
                        //              48 52
                        //              -----
                        //          _____ ____  ____
                        //          45 47 - - 55 -
                        //          ----- ----  ----
                        // We now apply fix second to obtain the following:
                        //
                        //              _____
                        //              47 52
                        //              -----
                        //          _____ ____  ____
                        //          45 -  48 -  55 -
                        //          ----- ----  ----
                        //

                        return fixMiddle(node);
                    } else {
                        success = false;
                        return false;
                    }
                }
                // If the key is greater than K2 we start recursion to the right child of the node
                else if (key.compareTo(index,node.getK2()) > 0) {
                    //If the recursive call returns true, the node needs to be merged.
                    if (deleteRec(node.getRight())) {
                        fixRight(node);
                        return false;
                    }
                    else {
                        success = false;
                        return false;
                    }
                }

                // The key is equal to K2
                else {
                    // Example: deleting 52
                    //
                    //              _____
                    //              48 52
                    //              -----
                    //          _____ ____  ____
                    //          45 47 50 51 55 -
                    //          ----- ----  ----
                    //
                    // After we apply inOrderSuccessor()
                    //              _____
                    //              48 55
                    //              -----                 succ = 55
                    //          _____ ____  ____
                    //          45 47 50 51 - -
                    //          ----- ----  ----
                    //
                    if (inOrderSuccessor(node.getRight())) {
                        node.setK2(succ);
                        //The node is a 1-structure.Node leaf --> merge
                        if (fixInOrderSuccessor(node.getRight())) {
                            fixRight(node);
                            return false;
                        }
                        else{
                            success = false;
                            return false;
                        }
                    }
                    //If inOrderSuccessor returns false it means the middle node is a 2-structure.Node; we apply the following:
                    else {
                        // Example: deleting 52
                        //
                        //              _____
                        //              48 52
                        //              -----
                        //          _____ ____  ____
                        //          45 47 50 51 55 57
                        //          ----- ----  ----
                        //
                        // After we apply inOrderSuccessor()
                        //              _____
                        //              48 55
                        //              -----                 succ = 55
                        //          _____ ____  ____
                        //          45 47 50 51 57 -
                        //          ----- ----  ----
                        //
                        node.setK2(succ);
                    }
                }
            }
            // If the node is a 2-node
            else {
                // We start recursion to the middle child of the node
                if (deleteRec(node.getMiddle())) {
                    //If the recursive call returns true, the node needs to be merged.
                    return fixMiddle(node);
                }
                else{
                    success = false;
                    return false;
                }
            }
        }
        // Case the node is a leaf
        else {
            // If the key equals K1
            if (key.compareTo(index,node.getK1()) == 0) {
                // Example: deleting 48
                //
                //              _____
                //              48 52
                //              -----
                //              _____
                //              52 -
                //              -----
                //
                node.setK1(node.getK2());
                node.setK2(null);
                return (node.getK1() == null);
            }

            // If the key equals K2 or there isn't K2 (the key does not exist)
            else if ((node.getK2() != null) && (key.compareTo(index,node.getK2())  == 0)) {
                // Example: deleting 52
                //
                //              _____
                //              48 52
                //              -----
                //              _____
                //              48 -
                //              -----
                //
                node.setK2(null);
            }
        }

        return false;
    }

    /**
     * Method that prints the tree by levels
     */
    public void displayTree (){
        LinkedList <Node> cue;
        Node aux;
        if (root != null){
            cue = new LinkedList<>();
            cue.push(root);
            while (!cue.isEmpty()){
                aux = cue.poll();
                if (aux.hasLeftNode()) cue.addLast(aux.getLeft());
                if (aux.hasMiddleNode()) cue.addLast(aux.getMiddle());
                if (aux.hasRightNode()) cue.addLast(aux.getRight());
                System.out.println(aux.toString());

            }
        } else {
            System.out.println("Empty Tree!");
        }
    }

    /**
     * Method that returns the height of the tree
     * @return height
     */
    public int height (){
        if (root == null){
            return 0;
        } else{
            Node aux = root;
            int q = 0;
            while (aux != null){
                aux = aux.getLeft();
                q++;
            }
            return q;
        }
    }



    @Override
    protected boolean add(TableRow tableRow) {
        this.key = tableRow;
        success = true;
        // Case the tree is empty
        if (root == null) {
            root = new Node(this.key);
        }
        // Otherwise
        else {
            // If there has been a split, we need to create a new root node with a k1 value of key,
            // and its two more left childs as lNode and mNode;
            if (insertRec(root)) {
                root = new Node(this.key);
                root.setLeft(lNode);
                root.setMiddle(mNode);
            }
        }
        return success;
    }

    @Override
    protected void select(TableRowRestriction restrictions) {
        if (root != null) {
            selectRec(root,restrictions);
        }
    }

    private void selectRec (Node n, TableRowRestriction restriction) {
        if (restriction.test(n.getK1())){
            System.out.println(n.getK1().toString());
        }
        if (n.getK2() != null && restriction.test(n.getK2())){
            System.out.println(n.getK2().toString());
        }
        if (n.hasLeftNode()) {
            selectRec(n.getLeft(),restriction);
        }
        if (n.hasMiddleNode()) {
            selectRec(n.getMiddle(),restriction);
        }
        if (n.hasRightNode()) {
            selectRec(n.getRight(),restriction);
        }
    }

    @Override
    protected boolean update(String field, TableRow row) {
        LinkedList<TableRow> myRow;
        if (root == null) return false;
        else {
            myRow = findRec(root,row);
        }
        if (myRow == null) return false;
        else {
            myRow.addFirst(row);
            return true;
        }
    }

    private LinkedList<TableRow> findRec (Node n, TableRow row) {
        if (row.compareTo(index,n.getK1()) == 0) {
            return n.getListK1();
        }
        if (n.getK2() != null) {
            if (row.compareTo(index,n.getK2()) == 0) {
                return n.getListK2();
            }
        }
        if (row.compareTo(index,n.getK1()) == -1) {
            if (n.hasLeftNode()){
                return findRec(n.getLeft(),row);
            }
            else {
                return null;
            }
        }
        else {
            if (n.getK2() == null || n.getK2() != null && row.compareTo(index, n.getK2()) == -1) {
                if (n.hasMiddleNode()) {
                    return findRec(n.getMiddle(), row);
                } else {
                    return null;
                }
            } else {
                if (n.hasRightNode()) {
                    return findRec(n.getRight(), row);
                } else {
                    return null;
                }
            }
        }
    }

    @Override
    protected boolean remove(String field, Object value) {
        success = true;
        this.key = new TableRow();
        key.setColumn(field, value);
        if (root != null) {
            if (deleteRec(root)) {
                root = root.getLeft();
            }
        }

        return success;

    }

    /**
     * Method that returns the number of nodes that the tree has.
     * @return number of nodes
     */
    @Override
    protected long size() {
        if (root == null){
            return 0;
        } else{
            LinkedList <Node> cue = new LinkedList<>();
            Node aux;
            long i = 0;
            cue.push(root);
            while (!cue.isEmpty()){
                aux = cue.poll();
                if (aux.hasLeftNode()) cue.addLast(aux.getLeft());
                if (aux.hasMiddleNode()) cue.addLast(aux.getMiddle());
                if (aux.hasRightNode()) cue.addLast(aux.getRight());
                if (aux.getK2()!= null) i+=2;
                else i++;
            }
            return i;
        }
    }

    @Override
    public TableRow getRow(Object indexValue) {
        TableRow row = new TableRow();
        row.setColumn(index,indexValue);
        LinkedList<TableRow> aux = findRec(root,row);
        if (aux == null) return null;
        return aux.getLast();
    }

    @Override
    public boolean exportToCSV(String path) {
        LinkedList<Node> nivells = new LinkedList<>();
        StringBuilder data = new StringBuilder();

        if (root == null) {
            return false;
        } else {

            for (String s : root.getK1().getColumnNames()) {
                data.append(s).append(",");
            }
            data.deleteCharAt(data.length() - 1);
            data.append(System.lineSeparator());

            nivells.push(root);
            while (!nivells.isEmpty()) {
                Node n = nivells.poll();

                for (Object o : n.getK1().getFields()) {
                    data.append(o.toString()).append(",");
                }


                if (n.getK2() != null) {

                    data.deleteCharAt(data.length() - 1); //borrar coma, linea magica
                    data.append(System.lineSeparator());

                    for (Object o : n.getK2().getFields()) {
                        data.append(o.toString()).append(",");
                    }
                }

                data.deleteCharAt(data.length() - 1); //borrar coma, linea magica
                data.append(System.lineSeparator());
                if (n.hasLeftNode()) nivells.addLast(n.getLeft());
                if (n.hasMiddleNode()) nivells.addLast(n.getMiddle());
                if (n.hasRightNode()) nivells.addLast(n.getRight());
            }
        }

        try (PrintWriter pw = new PrintWriter(path)) {
            pw.write(data.toString());
            return true;
        } catch (FileNotFoundException e) {
            return false;
        }
    }


    private static class Node {
        private Node left;
        private Node middle;
        private Node right;
        private LinkedList<TableRow> k1;
        private LinkedList<TableRow> k2;


        private Node (TableRow k1){
            this.k1 = new LinkedList<>();
            this.k1.add(k1);
        }

        private LinkedList<TableRow> getListK1 () {
            return k1;
        }
        private LinkedList<TableRow> getListK2() {
            return k2;
        }
        private Node getLeft() {
            return left;
        }

        private void setLeft(Node left) {
            this.left = left;
        }

        private Node getMiddle() {
            return middle;
        }

        private void setMiddle(Node middle) {
            this.middle = middle;
        }

        private Node getRight() {
            return right;
        }

        private void setRight(Node right) {
            this.right = right;
        }

        private TableRow getK1() {
            return k1.getLast();
        }

        private void setK1(TableRow k1) {
            this.k1.add(k1);
        }

        private TableRow getK2() {
            if (this.k2 != null) {
                return k2.getLast();
            } else {
                return null;
            }
        }

        private void setK2(TableRow k2) {
            if (this.k2 == null) this.k2 = new LinkedList<>();
            this.k2.add(k2);
        }

        private boolean isLeaf (){
            if (left == null && middle == null && right == null) return true;
            return false;
        }

        private boolean hasRightNode(){
            return right != null;
        }

        private boolean hasLeftNode(){
            return left != null;
        }

        private boolean hasMiddleNode(){
            return middle != null;
        }


        @Override

        public String toString (){
            return "(" + this.getK1() + "," + this.getK2() + ")";
        }
    }

}