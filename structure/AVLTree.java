package structure;

import base.TableDataStructure;
import base.TableRow;
import base.TableRowRestriction;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * @author Carlota & Dani
 */
public class AVLTree extends TableDataStructure {

    private Nodo raiz;

    private int size;

    public AVLTree() {
    }

    public AVLTree(String index) {
        setIndex(index);
    }

    @Override
    protected boolean add(TableRow tableRow) {
        return insertar(tableRow);
    }

    @Override
    protected void select(TableRowRestriction restrictions) {
        select(raiz, restrictions);
    }

    private void select(Nodo nodo, TableRowRestriction r) {
        if (nodo != null) {
            select(nodo.izq, r);
            if (r.test(nodo.valor())) {
                System.out.println(nodo.valor().toString());
            }
            select(nodo.der, r);
        }
    }

    @Override
    protected boolean update(String field, TableRow row) {
        Nodo fila = buscar(row.getValue(index));

        //fila.addNewRow().setColumn(field, row.getValue(field));
        if (fila != null) {
            fila.addNewRow(row);
            //fila.setColumn(field, row.getValue(field));
        }
        return true;
    }

    @Override
    protected boolean remove(String field, Object value) {
        if (field.equals(index)) {
            Object toDelete = ((TableRow) value).getValue(field);
            return eliminar(field, toDelete);
        }
        return false;
    }

    @Override
    protected long size() {
        return size;
    }

    @Override
    public TableRow getRow(Object indexValue) {
        Nodo resultado = buscar(indexValue);
        return resultado == null ? null : resultado.valor();
    }

    @Override
    public List<TableRow> getHistoric(Object indexValue) {
        return buscar(indexValue).valor;
    }

    private boolean insertar(TableRow valor) {
        if (raiz == null) {
            raiz = new Nodo(valor);
            size++;
            return true;
        } else {
            try {
                raiz = raiz.insertar(valor);
                size++;
                return true;
            } catch (Exception e) {
                //duplicado
                return false;
            }
        }
    }

    private boolean eliminar(String field, Object valor) {
        if (raiz != null) {
            if (raiz.valor().getValue(field).equals(valor) && size == 1) {
                size--;
                raiz = null;
                return true;
            }
            Nodo nodoEliminado = raiz.eliminar(field, valor);
            if (nodoEliminado == null) {
                return false;
            }
            raiz = nodoEliminado;
            size--;
            if (size == 0) {
                this.raiz = null;
            }
            return true;
        } else {
            return false;
        }
    }

    protected Nodo buscar(Object index) {
        try {
            return buscar(raiz, index);
        } catch (NoSuchElementException ex) {
            return null;
        }
    }

    private Nodo buscar(Nodo nodo, Object valor) throws NoSuchElementException {
        if (nodo == null) throw new NoSuchElementException();
        int result = nodo.valor().compareTo(getIndex(), valor);
        if (result > 0) {
            nodo = buscar(nodo.izq, valor);
        } else if (result < 0) {
            nodo = buscar(nodo.der, valor);
        }
        return nodo;
    }

    @Override
    public boolean exportToCSV(String path) {
        LinkedList<Nodo> preordre = new LinkedList<>();
        StringBuilder data = new StringBuilder();

        if (raiz == null) {
            return false;
        } else {

            for (String s : raiz.valor().getColumnNames()) {
                data.append(s).append(",");
            }
            data.deleteCharAt(data.length() - 1);
            data.append(System.lineSeparator());

            preordre.push(raiz);
            while (!preordre.isEmpty()) {
                Nodo n = preordre.pop();

                for (Object o : n.valor().getFields()) {
                    data.append(o.toString()).append(",");
                }

                data.deleteCharAt(data.length() - 1); //borrar coma, linea magica
                data.append(System.lineSeparator());
                if (n.izq() != null) preordre.push(n.izq());
                if (n.der() != null) preordre.push(n.der());
            }
        }

        try (PrintWriter pw = new PrintWriter(path)) {
            pw.write(data.toString());
            pw.close();
            return true;
        } catch (FileNotFoundException e) {
            return false;
        }
    }

    protected List<TableRow> getAllRows() {
        return recorrer(raiz, new LinkedList<>());
    }

    private List<TableRow> recorrer(Nodo n, List<TableRow> list) {
        if (n != null) {
            if (n.izq != null) recorrer(n.izq, list);
            if (n.der != null) recorrer(n.der, list);
            list.add(n.valor());
        }
        return list;
    }

    public Nodo getRaiz() {
        return raiz;
    }

    public class Nodo {

        //private TableRow valor;
        private LinkedList<TableRow> valor; //lista para el hisorico
        private Nodo izq; // izq child
        private Nodo der; // der child
        private int altura; //al final tuve que hacerlo

        private Nodo(TableRow valor) {
            this.valor = new LinkedList<>();
            //this.valor = valor;
            this.valor.addLast(valor);
        }

        private Nodo rotarRR(Nodo a) {
            Nodo b = a.der;

            a.der = b.izq;
            b.izq = a;

            a.altura = Math.max(a.alturaIzq(), a.alturaDer()) + 1;
            b.altura = Math.max(b.alturaDer(), a.altura);

            return b;
        }

        private Nodo rotarLL(Nodo b) {
            Nodo a = b.izq;

            b.izq = a.der;
            a.der = b;

            b.altura = Math.max(b.alturaIzq(), b.alturaDer()) + 1;
            a.altura = Math.max(a.alturaIzq(), b.altura) + 1;

            return a;
        }

        private Nodo rotarRL(Nodo a) {
            a.der = rotarLL(a.der);
            return rotarRR(a);
        }

        private Nodo rotarLR(Nodo a) {
            a.izq = rotarRR(a.izq);
            return rotarLL(a);
        }

        private Nodo balance(Nodo nodo) {
            if (nodo == null) return nodo;

            int balance = nodo.alturaIzq() - nodo.alturaDer();

            if (balance > 1) {
                //caso desbalance en izquierda
                if ((nodo.izq == null ? 0 : nodo.izq.alturaIzq()) >= (nodo.izq == null ? 0 : nodo.izq.alturaDer())) {
                    //caso LL
                    nodo = rotarLL(nodo);
                } else {
                    //caso LR
                    nodo = rotarLR(nodo);
                }
            } else if (balance < -1) {
                //caso desbalance en derecha
                if ((nodo.der == null ? 0 : nodo.der.alturaDer()) >= (nodo.der == null ? 0 : nodo.der.alturaIzq())) {
                    //caso RR
                    nodo = rotarRR(nodo);
                } else {
                    //caso RL
                    nodo = rotarRL(nodo);
                }
            }

            return nodo;
        }

        public Nodo insertar(TableRow valor) throws Exception {
            int cmp = valor.compareTo(index, this.valor());
            if (cmp < 0) {
                if (izq != null) {
                    izq = izq.insertar(valor);
                } else {
                    izq = new Nodo(valor);
                }
            } else if (cmp > 0) {
                if (der != null) {
                    der = der.insertar(valor);
                } else {
                    der = new Nodo(valor);
                }
            } else {
                //duplicado
                throw new Exception();
            }
            altura = Math.max(alturaIzq(), alturaDer()) + 1;
            return balance(this);
        }

        private Nodo eliminar(String field, Object valor, Nodo nodo) {
            int cmp = nodo.valor().compareTo(field, valor);

            if (cmp > 0) {
                if (nodo.izq == null) return null;

                nodo.izq = eliminar(field, valor, nodo.izq);
            } else if (cmp < 0) {
                if (nodo.der == null) return null;

                nodo.der = eliminar(field, valor, nodo.der);
            } else if (nodo.izq != null && nodo.der != null) {
                //dos hijos
                nodo.valor = nodo.encontrarMinimo(nodo.der).valor; //primer inordre de la derecha
                nodo.der = eliminar(field, nodo.valor().getValue(field), nodo.der);
            } else {
                nodo = nodo.izq != null ? nodo.izq : nodo.der;
            }
            return balance(nodo);
        }

        private Nodo encontrarMinimo(Nodo nodo) {
            while (nodo.izq != null)
                nodo = nodo.izq;

            return nodo;
        }

        private boolean buscar(String field, Object valor) {
            int cmp = valor().compareTo(field, valor);
            boolean encontrado = true;
            if (cmp > 0) {
                if (izq == null) return false;
                encontrado = izq.buscar(field, valor);
            } else if (cmp < 0) {
                if (der == null) return false;
                encontrado = der.buscar(field, valor);
            }
            return encontrado;

        }

        private Nodo eliminar(String field, Object valor) {
            if (buscar(field, valor))
                return eliminar(field, valor, this);

            return null;
        }

        public TableRow valor() {
            return valor.isEmpty() ? null : valor.getFirst();
        }

        public Nodo izq() {
            return izq;
        }

        public Nodo der() {
            return der;
        }

        public int altura() {
            return altura;
            //return Math.max(izq == null ? 0 : izq.altura(), der == null ? 0 : der.altura()) + 1;
        }

        private int alturaIzq() {
            return izq == null ? 0 : izq.altura();
        }

        private int alturaDer() {
            return der == null ? 0 : der.altura();
        }

        public TableRow addNewRow(TableRow newFields) {
            TableRow nueva = new TableRow(new LinkedHashMap<>(newFields.getContent())); //anyadir nueva fila
            valor.addFirst(nueva);
            return nueva;
        }
    }
}
