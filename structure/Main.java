package structure;

import base.DataType;
import base.Table;
import base.TableRow;
import base.TableRowRestriction;
import hasher.HashInt1;
import hasher.HashInt2;
import hasher.HashText1;
import hasher.HashText2;
import model.Model;
import util.MainMenu;
import util.TableMenu;

import java.util.*;

/**
 * @author Carlota & Dani
 */
public class Main {
    private static Scanner sc;
    private static final String [] OPTIONS_1 = {"Enter a name for the table: ",
                                        "Select a data structure for the table",
                                        "Enter a column name for the new table"};
    private static final String [] QUESTIONS_NEW_COLUMN = {"Which kind of data stores this column? ",
                                                            "Select a column from the list to use it as index: ",
                                                            "Which field?: ",
                                                            " added into the system."};
    private static final String ADDED_CORRECTLY = "Column added correctly.";
    private static final String WANT_ANOTHER_COLUMN = "Would you like to add another column? [Y/N]";
    private static final String FORMAT_INCORRECTE = "Format incorrecte. Siusplau, introdueix una opció vàlida.";
    private static final char [] ANSWERS = {'Y', 'N'};
    private static final String [] DATA_STRUCTURES = {"1. AVL Tree", "2. 2-3 Tree", "3. Hash Table"};
    private static final String ERROR_INSERIR_COLUMNA = "Error a l'inserir la nova columna. " +
            "Comprova que la columna no exiteix i/o el tipus de dades a emmagatzemar existeix.";

    private static final String TAULA_A_CONSULTAR = "Select the table you want to manange:";
    private static final String WHICH_TABLE = "Which table? ";
    private static final String ROW_ADDED = "Row added correctly";
    private static final String ROW_NOT_ADDED = "Couldn't add row correctly";
    private static final String DASHES = "----------------------------------------------------------------------------";
    private static final String [] OPTIONS_23 = {"1. Stablish condition", "2. Execute select", "3. Exit"};
    private static final String [] RESTRICTIONS = {"1. Less than ", "2. Equals to ", "3. Greater than "};
    private static final String LAST = "Select an option: ";
    private static final String MODIFY = "Do you want to modify the column '";
    private static final String AREUSURE = "Are you sure you want to delete this row? [Y/N]: ";
    private static final String IMPORT = "CSV Import for table ";
    private static final String ENTER = "Enter the name of the CSV file: ";
    private static final String EXPORT = "Generating CSV file for table ";
    private static final String SELECT_TABLE = "Select a table: ";

    public static void main(String[] args) {
        int opt = 0;
        sc = new Scanner(System.in);

        //Demanem una opcio a l'usuari
        while (opt != 5) {

            while (opt < 1 || opt > 5) {
                new MainMenu();
                try {
                    opt = sc.nextInt();

                    if (opt < 1 || opt > 5) throw new NumberFormatException();

                } catch (NumberFormatException | InputMismatchException e) {
                    System.out.println(FORMAT_INCORRECTE);
                }
                sc.nextLine();
            }
            //Cridem els metodes corresponents
            switch (opt) {

                case 1:
                    es1();
                    break;

                case 2:
                    es2();
                    break;

                case 3:
                    es3();
                    break;

                case 4:
                    es4();
                    break;

                case 5:
                    System.out.println("Bye!");
                    System.exit(0);
                    break;
            }
            opt = 0;
            System.out.println(System.lineSeparator());
        }

    }

    //Cas volem crear una taula
    private static void es1 () {
        Table taula = null;
        System.out.print(OPTIONS_1[0]);
        String tableName = sc.nextLine();
        while (Model.getInstance().containsTable(tableName)){
            System.out.println("Ja existeix una taula amb aquest nom. Si us plau, introdueix-ne un altre.");
            tableName = sc.nextLine();
        }

        int dataStructure = 0;

        while (dataStructure < 1 || dataStructure > 3) {

            System.out.println(OPTIONS_1[1] + " " + tableName + ": ");

            for (String s : DATA_STRUCTURES){
                System.out.println(s);
            }
            dataStructure = comprova13();
            sc.nextLine();
        }

        switch (dataStructure) {

            case 1:
                taula = new Table(tableName,new AVLTree());
                break;

            case 2:
                taula = new Table(tableName,new TwoThreeTree());
                break;

            case 3:
                System.out.println("Quin data type serà l'índex de la teva taula ?");
                System.out.println("1. INT");
                System.out.println("2. TEXT");

                int p = 0;
                do {
                    try {
                        p = sc.nextInt();
                    } catch (NumberFormatException | InputMismatchException e) {
                        e.printStackTrace();
                        System.out.println(p);
                        System.out.println(FORMAT_INCORRECTE);
                    }
                    sc.nextLine();
                } while (p < 1 && p > 2);

                int q = 0;
                System.out.print("Vols la funció de hash 1 o 2? ");
                do {
                    try {
                        q = sc.nextInt();
                    } catch (NumberFormatException | InputMismatchException e) {
                        System.out.println(FORMAT_INCORRECTE);
                        q = 0;
                    }
                    sc.nextLine();
                } while (q < 1 && q > 2);

                if (p == 1) {
                    if (q == 1) {
                        taula = new Table(tableName, new HashTable(new HashInt1()));
                    } else {
                        taula = new Table(tableName, new HashTable(new HashInt2()));
                    }
                } else {
                    if (q == 1) {
                        taula = new Table(tableName, new HashTable(new HashText1()));
                    } else {
                        taula = new Table(tableName, new HashTable(new HashText2()));
                    }
                }

                break;
        }

        boolean wantColum = true;
        boolean typeCorrect = false;
        String columnName;
        String columnType;

        while (wantColum) {
            System.out.println(OPTIONS_1[2] + " " + tableName + ": ");
            columnName = sc.nextLine();

            while (!typeCorrect) {
                System.out.println(QUESTIONS_NEW_COLUMN[0]);
                columnType = sc.nextLine();

                typeCorrect = columnTypeIsCorrect(columnType);
                if (!typeCorrect || !taula.addColumn(columnName,DataType.valueOf(columnType)))
                    System.out.println(ERROR_INSERIR_COLUMNA);
            }
            typeCorrect = false;
            System.out.println(ADDED_CORRECTLY);
            System.out.println(System.lineSeparator());
            System.out.println(WANT_ANOTHER_COLUMN);

            char answer = comprovaYN();

            if (answer == ANSWERS[1]) wantColum = false;

        }

        Model.getInstance().addTable(taula);
        System.out.println(System.lineSeparator());
        System.out.println(QUESTIONS_NEW_COLUMN[1]);
        List<String> columnNames = Model.getInstance().getColumnsTable(taula);
        boolean isCorrect = false;
        int numColumns = columnNames.size();

        while (!isCorrect) {

            for (int i = 0; i < numColumns; i++) {
                System.out.println(columnNames.get(i));
            }
            System.out.print(QUESTIONS_NEW_COLUMN[2]);

            String indexTable = sc.nextLine();

            try {
                taula.setIndex(indexTable);
                isCorrect = true;
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
                System.out.println(System.lineSeparator());
                isCorrect = false;
            }
        }

        System.out.println(System.lineSeparator());
        System.out.println("Table " + tableName + QUESTIONS_NEW_COLUMN[3]);
    }

    //Cas volem gestionar una taula
    private static void es2 () {
        System.out.println(TAULA_A_CONSULTAR);
        int numTaules = Model.getInstance().getTablesNames().size();
        ArrayList<String> tableNames = Model.getInstance().getTablesNames();

        boolean tableSelected = false;
        if (numTaules > 0) {
            int opt = 0;

            while (!tableSelected) {

                for (int i = 0; i < numTaules; i++) {
                    System.out.println((i + 1) + ". " + tableNames.get(i));
                }
                System.out.print(WHICH_TABLE);
                try {
                    opt = sc.nextInt();
                    if (opt < 1 || opt > numTaules) throw new NumberFormatException();
                    tableSelected = true;

                } catch (NumberFormatException | InputMismatchException e) {
                    System.out.println(FORMAT_INCORRECTE);
                    tableSelected = false;
                }
                sc.nextLine();
            }
            System.out.println(System.lineSeparator());
            startManagement (tableNames.get(opt - 1));
        }

    }

    //Cas volem visualitzar una taula
    private static void es3 () {
        LinkedList<Table> tables = Model.getInstance().getTables();
        int numTables = Model.getInstance().getNumTables();
        System.out.println(System.lineSeparator());
        TableRow aux = new TableRow();
        aux.setColumn("Column","Column");
        aux.setColumn("Data Type", "Data Type");

        for (int i = 0; i < numTables; i++) {
            System.out.println("Table " + tables.get(i).getName());
            System.out.println(DASHES);
            int q = tables.get(i).getColumnTypes().get(0).toString().equals("INT") ? 0 : 1;
            System.out.println(printColumns(aux,q));
            System.out.println(DASHES);
            List<String> names = tables.get(i).getColumnNames();
            List<DataType> dataTypes = tables.get(i).getColumnTypes();
            TableRow l = new TableRow();
            int numColumns = names.size();

            for (int j = 0; j < numColumns; j++) {
                TableRow c = new TableRow();
                c.setColumn(names.get(j),names.get(j));
                c.setColumn(dataTypes.get(j).toString(),dataTypes.get(j));
                System.out.println(c.toString());
            }

            System.out.println(System.lineSeparator());
            System.out.println("Number of rows in this table: " + tables.get(i).getRowsNumber() );


        }
    }

    //Cas volem veure l'historic
    private static void es4 () {
        LinkedList<Table> taules = Model.getInstance().getTables();
        int i = 1;

        for (Table t: taules) {
            System.out.println(printTable(t,i));
            i++;
        }
        System.out.print(SELECT_TABLE);
        int op = 0;
        boolean ok = false;
        try {
            op = sc.nextInt();
            ok = true;
            if (op < 1 || op > taules.size()) throw new NumberFormatException();
        } catch (NumberFormatException | InputMismatchException e){
            System.out.println(FORMAT_INCORRECTE);
            ok = false;
        }
        sc.nextLine();

        if (ok) {
            Table table = taules.get(op-1);
            String columnName = table.getIndex();
            String indexT = table.getColumnType(columnName).toString();
            System.out.print("Enter a " + indexT + " value for " + columnName + ": ");
            String value = sc.nextLine();
            Object id;
            if (indexT.equals("INT")) id = Integer.parseInt(value);
            else id = value;
            try {
                TableRow row = table.getRowByIndex(id);
                int numC = row.getColumnNames().size();
                List<String> columnNames = table.getColumnNames();
                System.out.println(DASHES);
                TableRow aux = new TableRow();
                for (int j = 0; j < numC; j++) {
                    aux.setColumn(columnNames.get(j),columnNames.get(j));
                }
                int q = table.getColumnTypes().get(0).toString().equals("INT") ? 0 : 1;
                System.out.println(printColumns(aux,q));
                System.out.println(DASHES);

                /*int numUpdates = row.getNumUpdates();
                for (int j = 0; j < numUpdates; j++) {
                    for (int b = 0; b < numC; b++) {
                        aux.setColumn(columnNames.get(b),row.getContentColumn(columnNames.get(b),j));
                    }
                    System.out.println(aux.toString());
                }*/
                for (TableRow fila : table.getHistoric(id)) {
                    System.out.println(fila.toString());
                }
                System.out.println(DASHES);

            } catch (IllegalArgumentException e){
                System.out.println("No existeix cap fila amb l'índex proporcionat!");
            }
        }
    }

    private static boolean columnTypeIsCorrect (String type) {
        try {
            DataType.valueOf(type);
            return true;
        } catch (IllegalArgumentException | NullPointerException e11) {
            return false;
        }

    }

    //Cas de gestionar una taula
    private static void startManagement (String table) {
        Table myTable = Model.getInstance().getTable(table);
        TableMenu tMenu = new TableMenu();
        int opt = 0;
        boolean optCorrect = false;

        //Demanem una opcio
        while (opt != 8) {
            System.out.println(myTable.getName() + " Table Management");
            tMenu.printTableMenu();

            while (!optCorrect) {
                try {
                    System.out.println(LAST);
                    opt = sc.nextInt();
                    if (opt < 1 || opt > 8) throw new NumberFormatException();
                    optCorrect = true;

                } catch (NumberFormatException | InputMismatchException e) {
                    System.out.println(FORMAT_INCORRECTE);
                    optCorrect = false;
                }
                sc.nextLine();
            }
            optCorrect = false;

            switch (opt) {
                //volem inserir
                case 1:
                    int numColumns = myTable.getColumnNames().size();
                    List<String> columns = myTable.getColumnNames();
                    String indexT = "";
                    String columnName = "";
                    TableRow tableRow = new TableRow();

                    for (int i = 0; i < numColumns; i++) {
                        columnName = columns.get(i);
                        indexT = myTable.getColumnType(columnName).toString();
                        System.out.print("Enter a " + indexT + " value for " + columnName + ": ");
                        String value = sc.nextLine();
                        String dataType = myTable.getColumnType(columnName).toString();
                        switch (dataType) {
                            case "TEXT":
                                tableRow.setColumn(columnName,value);
                                break;
                            case "INT":
                                tableRow.setColumn(columnName,Integer.valueOf(value));
                                break;
                            case "BOOLEAN":
                                tableRow.setColumn(columnName,Boolean.valueOf(value));
                                break;
                            case "LONG":
                                tableRow.setColumn(columnName,Long.valueOf(value));
                                break;
                            case "FLOAT":
                                tableRow.setColumn(columnName,Float.valueOf(value));
                                break;
                            case "DOUBLE":
                                tableRow.setColumn(columnName,Double.valueOf(value));
                                break;
                            case "CHAR":
                                tableRow.setColumn(columnName,value.charAt(0));
                                break;
                        }

                    }
                    if (myTable.addRow(tableRow)) {
                        System.out.println(ROW_ADDED);
                    } else {
                        System.out.println(ROW_NOT_ADDED);
                    }
                    break;
                //volem mostrar una table row indicant el seu index
                case 2:
                    List<String> columnNames = myTable.getColumnNames();
                    numColumns = columnNames.size();
                    TableRow aux = new TableRow();


                    for (int i = 0; i < numColumns; i++) {
                        columnName = columnNames.get(i);
                        aux.setColumn(columnName,columnName);
                    }

                    columnName = myTable.getIndex();
                    indexT = myTable.getColumnType(myTable.getIndex()).toString();
                    System.out.print("Enter a " + indexT + " value for " + columnName + ": ");
                    String value = sc.nextLine();
                    Object data;
                    if (indexT.equals("INT")) data = Integer.parseInt(value);
                    else data = value;
                    TableRowRestriction restriction = new TableRowRestriction();
                    restriction.addRestriction(columnName,data,2);

                    int l = myTable.getColumnTypes().get(0).toString().equals("INT") ? 0 : 1;
                    System.out.println(DASHES);
                    System.out.println(printColumns(aux,l));
                    System.out.println(DASHES);
                    myTable.selectRows(restriction);

                    break;
                //volem aplicar select
                case 3:
                    TableRowRestriction rowRestriction = new TableRowRestriction();
                    opt = 0;

                    while (opt != 3) {
                        System.out.println(System.lineSeparator());
                        System.out.println("------Select from " + myTable.getName() + "------" );

                        for (String s: OPTIONS_23) {
                            System.out.println(s);
                        }
                        System.out.println(System.lineSeparator());
                        opt = 0;
                        while (opt < 1 || opt > 3) {
                            System.out.print("Select an option: ");
                            opt = comprova13();
                            sc.nextLine();
                        }

                        switch (opt) {

                            case 1:
                                columnNames = myTable.getColumnNames();
                                System.out.print("Column? ");
                                columnName = sc.nextLine();
                                indexT = "";

                                if (!columnNames.contains(columnName)) {
                                    System.out.println("Error!");
                                    break;
                                } else {
                                    indexT = myTable.getColumnType(columnName).toString();
                                    System.out.print("Enter a " + indexT + " value for " + columnName + ": ");
                                    String val = sc.nextLine();
                                    Object d = null;

                                    switch (indexT) {
                                        case "TEXT":
                                            d = String.valueOf(val);
                                            break;
                                        case "INT":
                                            d = Integer.valueOf(val);
                                            break;
                                        case "BOOLEAN":
                                            d = Boolean.valueOf(val);
                                            break;
                                        case "LONG":
                                            d = Long.valueOf(val);
                                            break;
                                        case "FLOAT":
                                            d = Float.valueOf(val);
                                            break;
                                        case "DOUBLE":
                                            d = Double.valueOf(val);
                                            break;
                                        case "CHAR":
                                            d = val.charAt(0);
                                            break;
                                    }

                                    int opt2 = 0;

                                    while (opt2 < 1 || opt2 > 3) {
                                        for (String s : RESTRICTIONS) {
                                            System.out.println(s);
                                        }
                                        opt2 = comprova13();
                                        sc.nextLine();
                                    }

                                    switch (opt2) {
                                        case 1:
                                            rowRestriction.addRestriction(columnName, d, 1);
                                            break;

                                        case 2:
                                            rowRestriction.addRestriction(columnName, d, 2);
                                            break;

                                        case 3:
                                            rowRestriction.addRestriction(columnName, d, 3);
                                            break;
                                    }


                                }
                                break;

                            case 2:
                                columnNames = myTable.getColumnNames();
                                numColumns = columnNames.size();
                                aux = new TableRow();


                                for (int i = 0; i < numColumns; i++) {
                                    columnName = columnNames.get(i);
                                    aux.setColumn(columnName,columnName);
                                }

                                l = myTable.getColumnTypes().get(0).toString().equals("INT") ? 0 : 1;
                                System.out.println(DASHES);
                                System.out.println(printColumns(aux,l));
                                System.out.println(DASHES);
                                myTable.selectRows(rowRestriction);
                                rowRestriction = new TableRowRestriction();

                                break;

                            case 3:
                                //We return to the menu of table management
                                break;
                        }
                    }

                    break;
                //volem actualitzar una fila
                case 4:
                    boolean updated = false;
                    columnNames = myTable.getColumnNames();
                    columnName = columnNames.get(0);
                    numColumns = columnNames.size();
                    indexT = myTable.getColumnType(columnName).toString();
                    System.out.print("Enter a " + indexT + " value for " + columnName + ": ");
                    String v = sc.nextLine();
                    boolean exists = false;
                    TableRow tRow = null;
                    try {
                        if (indexT.equals("INT")) {
                            tRow = myTable.getRowByIndex(Integer.parseInt(v));
                        } else {
                            tRow = myTable.getRowByIndex(v);
                        }
                        exists = true;
                    } catch (IllegalArgumentException e) {
                        System.out.println(e.getMessage());
                        exists = false;
                    }

                    if (exists) {
                        TableRow nuevaRow = new TableRow();
                        nuevaRow.setColumn(myTable.getIndex(), tRow.getValue(myTable.getIndex()));
                        for (int i = 1; i < numColumns; i++) {
                            columnName = columnNames.get(i);
                            indexT = myTable.getColumnType(columnName).toString();
                            System.out.println(MODIFY + columnName + "' with " + tRow.getValue(columnName) + "? [Y/N]");

                            boolean wantUpdate;

                            char answer = comprovaYN();

                            if (answer == ANSWERS[0]){
                                wantUpdate = true;
                                updated = true;
                            }
                            else wantUpdate = false;

                            if (wantUpdate) {
                                System.out.print("Enter a " + indexT + " value for " + columnName + ": ");
                                String valor = sc.nextLine();
                                Object newValue = null;
                                //tRow.setColumn(columnName,newValue);
                                //myTable.updateRow(new TableRow().setColumn(myTable.getIndex(), tRow.getValue(myTable.getIndex()))
                                //        .setColumn(columnName, newValue));

                                switch (indexT) {
                                    case "TEXT":
                                        newValue = valor;
                                        break;
                                    case "INT":
                                        newValue = Integer.valueOf(valor);
                                        break;
                                    case "BOOLEAN":
                                        newValue = Boolean.valueOf(valor);
                                        break;
                                    case "LONG":
                                        newValue = Long.valueOf(valor);
                                        break;
                                    case "FLOAT":
                                        newValue = Float.valueOf(valor);
                                        break;
                                    case "DOUBLE":
                                        newValue = Double.valueOf(valor);
                                        break;
                                    case "CHAR":
                                        newValue = valor.charAt(0);
                                        break;
                                }


                                nuevaRow.setColumn(columnName, newValue);
                                System.out.println("Row modified correctly.");
                            } else {
                                nuevaRow.setColumn(columnName, tRow.getValue(columnName));
                            }
                        }
                        if (updated) {
                            myTable.updateRow(nuevaRow);
                            System.out.println("Updating table " + myTable.getName() + " with the changes done.");
                        }
                    }

                    break;
                //volem eliminar una fila
                case 5:
                    columnName = myTable.getIndex();
                    indexT = myTable.getColumnType(columnName).toString();
                    System.out.print("Enter a " + indexT + " value for " + columnName + ": ");
                    v = sc.nextLine();
                    exists = false;
                    tRow = null;
                    try {
                        if (indexT.equals("INT")) {
                            tRow = myTable.getRowByIndex(Integer.parseInt(v));
                        } else {
                            tRow = myTable.getRowByIndex(v);
                        }
                        exists = true;
                    } catch (IllegalArgumentException e) {
                        System.out.println(e.getMessage());
                        exists = false;
                    }

                    if (exists) {
                        columnNames = myTable.getColumnNames();
                        numColumns = columnNames.size();
                        aux = new TableRow();


                        for (int i = 0; i < numColumns; i++) {
                            columnName = columnNames.get(i);
                            aux.setColumn(columnName,columnName);
                        }

                        l = myTable.getColumnTypes().get(0).toString().equals("INT") ? 0 : 1;
                        System.out.println(DASHES);
                        System.out.println(printColumns(aux,l));
                        System.out.println(DASHES);

                        if (indexT.equals("INT")) {
                            System.out.println(myTable.getRowByIndex(Integer.parseInt(v)).toString());
                        } else {
                            System.out.println(myTable.getRowByIndex(v).toString());
                        }

                        System.out.print(AREUSURE);

                        char r = comprovaYN();
                        if (r == ANSWERS[0]) {
                            myTable.removeRow(tRow);
                            System.out.println("Row removed from the table " + myTable.getName());
                        } else {
                            System.out.println("No change was made");
                        }
                    }
                    break;
                //volem importar dades d'un fitxer csv
                case 6:
                    System.out.println(IMPORT + myTable.getName());
                    System.out.print(ENTER);
                    String file = sc.nextLine();
                    boolean fileOk = false;
                    int numRows = 0;
                    try {
                        numRows = myTable.importFromCSV(file,myTable);
                        fileOk = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("Hi ha hagut un error");
                        fileOk = false;
                    }

                    if (fileOk) {
                        System.out.println("Loading file data...");
                        System.out.println("Data loaded successfully. A total of " + numRows +
                                " new rows have been inserted into " + myTable.getName());
                    }

                    break;
                //volem exportar les dades a un fitxer csv
                case 7:
                    System.out.println(EXPORT + myTable.getName() + " ...");
                    myTable.exportToCSV(myTable.getName()+".csv");
                    System.out.println(myTable.getName() + ".csv file generated successfully with a total of "
                            + myTable.getRowsNumber() + " rows." );
                    break;
                //volem tornar al menu
                case 8:
                    //Do nothing. Automaticament tornem al menu principal.
                    break;
            }
            System.out.println(System.lineSeparator());

        }
    }

    /**
     * Comprova 'Y' | 'N'
     * @return resultat de la comprovacio
     */
    private static char comprovaYN () {
        char answer = 'n';
        while (answer !=  ANSWERS[0] && answer != ANSWERS[1]) {

            try {
                String aux2 = sc.nextLine();
                answer = aux2.charAt(0);
                if (aux2.length() > 1 || answer != ANSWERS[0] && answer != ANSWERS[1]) throw new
                        IllegalArgumentException("Format incorrecte");

            } catch (IllegalArgumentException e12) {
                System.out.println(FORMAT_INCORRECTE);
            }
        }

        return answer;

    }

    /**
     * Comprova 1,2,3
     * @return resultat de la comprovacio
     */
    private static int comprova13 () {
        int opt2 = 0;
        try {
            opt2 = sc.nextInt();
            if (opt2 < 1 || opt2 > 3) throw new NumberFormatException();
        } catch (NumberFormatException | InputMismatchException e) {
            System.out.println(FORMAT_INCORRECTE);
        }
        return opt2;
    }

    /**
     * Mostra una taula per terminal
     * @param table taula a mostrar
     * @param i nombre de la taula
     * @return String amb el contingut a mostrar
     */
    private static String printTable (Table table, int i) {
        StringBuilder sb = new StringBuilder();

        sb.append(DASHES + "\n");
        sb.append(String.format("%-5s ", i + "."));
        sb.append(String.format("%-5s ", "Table: " + table.getName()));
        sb.append("\n");
        sb.append(String.format("%-5s ", ""));
        sb.append(String.format("%-5s ", "Rows: " + table.getRowsNumber()));
        sb.append("\n");
        sb.append(DASHES);
        sb.append("\n");
        return sb.toString();

    }

    /**
     * Imprimeix les capçaleres de les taules
     * @param row row amb la capçalera
     * @param indexType data type de l'index
     * @return String amb la informació a mostrar
     */
    private static String printColumns (TableRow row, int indexType) {
        HashMap<String, Object> content = row.getContent();
        StringBuilder sb = new StringBuilder();
        int i = 0;

        for (Map.Entry<String, Object> entry : content.entrySet()) {

            Object value = entry.getValue();
            if (i == 0) {
                if (indexType == 0) {
                    sb.append(String.format("%-10s ", value));
                } else {
                    sb.append(String.format("%-20s ", value));
                }
                i++;
            }
            else {
                if (value instanceof String)
                    sb.append(String.format("%-20s ", value));

                else sb.append(String.format("%-10s ", value));
            }
        }

        return sb.toString();

    }
}
