package structure;

import base.TableDataStructure;
import base.TableRow;
import base.TableRowRestriction;
import util.HashFunction;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Carlota & Dani
 */
public class HashTable extends TableDataStructure {

    private AVLTree[] table;
    private HashFunction hasher;
    private long size;

    public HashTable(HashFunction hasher) {
        table = new AVLTree[65536];
        this.hasher = hasher;
    }

    @Override
    protected boolean add(TableRow tableRow) {
        AVLTree tree = getTreeFor(tableRow.getValue(index));
        TableRow ret = tree.getRow(tableRow.getValue(index));
        if (ret == null) {
            if (tree.add(tableRow)) {
                size++;
                return true;
            }
            return false;
        } else {
            //pues al ser una tabla de hash, voy a actualizar el contenido en caso de que ya exista
            //no se especifica nada en el enunciado
            ret.setContent((LinkedHashMap<String, Object>) tableRow.getContent());
            return true;
        }
    }

    private AVLTree getNodeFor(Object indexValue) {
        int hashedIndex = hasher.hash(indexValue);
        return table[hashedIndex];
    }

    private AVLTree getTreeFor(Object indexValue) {
        AVLTree nodo;
        int hashedIndex = hasher.hash(indexValue);
        nodo = table[hashedIndex];

        if (nodo == null) { //crear arbol en caso que sea nulo
            nodo = table[hashedIndex] = new AVLTree(index);
        }

        return nodo;
    }

    @Override
    protected void select(TableRowRestriction restrictions) {
        for (AVLTree nodo : table) {
            if (nodo != null) //recorrer tabla entera
                nodo.select(restrictions);
        }
    }

    @Override
    protected boolean update(String field, TableRow row) {
        return getTreeFor(row.getValue(field)).update(field, row);
    }

    @Override
    protected boolean remove(String field, Object value) {
        if (field.equals(index)) {
            return getTreeFor(((TableRow) value).getValue(index)).remove(field, value);
        }
        return false;
    }

    @Override
    protected long size() {
        return size;
    }

    @Override
    public TableRow getRow(Object indexValue) {
        AVLTree node = getNodeFor(indexValue);
        if (node == null) return null;
        return node.buscar(indexValue).valor();
    }

    @Override
    public List<TableRow> getHistoric(Object indexValue) {
        AVLTree nodo = getNodeFor(indexValue);
        return nodo == null ? new LinkedList<>() : nodo.getHistoric(indexValue);
    }

    @Override
    public boolean exportToCSV(String path) {
        try {
            BufferedWriter bw = Files.newBufferedWriter(Paths.get(path));
            boolean escritoColumnas = false;
            for (AVLTree nodo : table) {
                if (nodo != null) {
                    if (!escritoColumnas) {
                        escritoColumnas = true;
                        bw.write(nodo.getRaiz().valor().getColumnNamesCsv() + System.lineSeparator());
                    }
                    for (TableRow row : nodo.getAllRows()) {
                        bw.write(row.csvFormat() + System.lineSeparator());
                    }
                }
            }
            bw.close();
        } catch (IOException e) {
            return false;
        }

        return true;
    }
}
