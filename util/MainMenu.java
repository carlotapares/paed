package util;

/**
 * @author Carlota & Dani
 */
public class MainMenu {

    private final String [] OPTIONS = {"-----DBMS-----","1. Create table",
                                    "2. Manage table",
                                    "3. Visualize tables",
                                    "4. Visualize historial data",
                                    "5. Shut down"};

    private final String LAST = "Select an option: ";

    public MainMenu() {
        int numOpcions = OPTIONS.length;

        for (String s : OPTIONS) {
            System.out.println(s);
        }

        System.out.println(System.lineSeparator());
        System.out.print(LAST);

    }
}
