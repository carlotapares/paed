package util;

/**
 * @author Carlota & Dani
 */
public class TableMenu {

    private final String [] OPTIONS = {"1. Insert",
                                        "2. Show row by index",
                                        "3. Select",
                                        "4. Update row",
                                        "5. Remove row by index",
            "6. Import from CSV",
            "7. Export to CSV",
                                        "8. Main menu"};


    public TableMenu() {};

    public void printTableMenu() {
        for (String s : OPTIONS) {
            System.out.println(s);
        }

        System.out.println(System.lineSeparator());
    }
}
