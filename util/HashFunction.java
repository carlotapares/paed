package util;

/**
 * @author Carlota & Dani
 */
public interface HashFunction {
    int R = 65521;

    int hash(Object o);
}
