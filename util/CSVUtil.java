package util;

import base.Table;

/**
 * @author Carlota & Dani
 */
public interface CSVUtil {

    boolean exportToCSV(String path);

    int importFromCSV(String path, Table table);
}
