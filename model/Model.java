package model;

import base.Table;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Carlota & Dani
 * Classe que gestiona totes les taules del programa
 */
public class Model {

    private static Model instance;
    private LinkedList<Table> taules;
    private Model() {
        taules = new LinkedList<>();
    }

    public static Model getInstance() {
        if (instance == null) {
            instance = new Model();
        }
        return instance;
    }

    public void addTable(Table taula) {
        taules.add(taula);
        taules.sort(Comparator.comparing(Table::getName));
    }

    public LinkedList<Table> getTables() {
        return taules;
    }

    public Table getTable(String tableName) {
        Table t = new Table(tableName,null);
        return taules.get(taules.indexOf(t));
    }

    public List<String> getColumnsTable (Table t) {
        return taules.get(taules.indexOf(t)).getColumnNames();
    }

    public ArrayList<String> getTablesNames () {
        ArrayList<String> names = new ArrayList<>();

        for (Table t : taules) {
            names.add(t.getName());
        }
        return names;
    }

    public int getNumTables () {
        return taules.size();
    }

    public boolean containsTable (String name){
        Table t = new Table(null,null);
        t.setName(name);
        return taules.contains(t);
    }
}
