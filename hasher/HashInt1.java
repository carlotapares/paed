package hasher;

import util.HashFunction;

import java.nio.ByteBuffer;

/**
 * @author Carlota & Dani
 */
public class HashInt1 implements HashFunction {

    @Override
    public int hash(Object o) {
        int i = (int) o;
        byte [] bytes = intToBytes(i);
        int hash = 0;

        for (byte b : bytes) {
            hash += b;
            hash += (hash << 10);
            hash ^= (hash >>> 6);
        }
        hash += (hash << 3);
        hash ^= (hash >>> 11);
        hash += (hash << 15);

        hash = Math.abs(hash%R);

        return hash;


    }

    private static byte[] intToBytes(int x) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putInt(x);
        return buffer.array();
    }
}
