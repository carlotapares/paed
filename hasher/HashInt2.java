package hasher;

import util.HashFunction;

import java.nio.ByteBuffer;

/**
 * @author Carlota & Dani
 */
public class HashInt2 implements HashFunction {

    @Override
    public int hash(Object o) {
        int i = (int) o;
        byte[] bytes = intToBytes(i);
        int hash = 0;
        long g;

        for (int j = 0; j < 8; j++) {
            hash = (hash << 4) + bytes[j];
            g = hash & 0xf0000000L;

            if (g != 0)
            {
                hash ^= g >> 24;
            }

            hash &= ~g;
        }
        hash = hash%R;

        return hash;
    }

    private static byte[] intToBytes(int x) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
        buffer.putInt(x);
        return buffer.array();
    }
}
