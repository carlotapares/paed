package hasher;

import util.HashFunction;

/**
 * @author Carlota & Dani
 */
public class HashText1 implements HashFunction {

    @Override
    public int hash(Object o) {
        String s = String.valueOf(o);
        int ret = 0;
        int h = 0;
        char[] chars = s.toCharArray();
        if (chars.length > 0) {
            for (char c : chars) {
                h = 31 * h + c;
            }
            ret = h;
        }
        return Math.abs(ret % R);
    }
}