package hasher;

import util.HashFunction;

/**
 * @author Carlota & Dani
 */
public class HashText2 implements HashFunction {

    @Override
    public int hash(Object o) {
        //hash basado en DJB2 https://en.wikipedia.org/wiki/DJB2
        String s = String.valueOf(o);
        int hash = 5381;
        for (char c : s.toCharArray()) {
            hash = ((hash << 5) + hash) + c;
        }
        return Math.abs(hash % R);
    }
}
