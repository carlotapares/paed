package base;

import util.CSVUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by Albertpv on 15/01/17.
 * <p>
 * <p>Defineix la base de l'estructura de qualsevol taula del sistema.</p>
 *
 * @author Programació Avançada i Estructura de Dades (PAED)
 *         Universitat La Salle Ramon Llull
 * @see Table
 */
public abstract class TableDataStructure implements CSVUtil {

    /**
     * Representa el nom de la columna que serà utilitzada per organitzar l'estructura.
     */
    protected String index;


    /**
     * @return El nom de l'índex.
     */
    public String getIndex() {
        return index;
    }

    /**
     * Estableix el nom del camp pel qual s'organitza l'estructura.
     *
     * @param field El nom del camp que serà l'índex.
     */
    public void setIndex(String field) {
        this.index = field;
    }

    /**
     * Afegeix una nova fila dins de l'estructura utilitzada per una taula.
     *
     * @param tableRow La nova fila a afegir.
     * @return true si s'ha pogut afegir, false en cas contrari.
     */
    protected abstract boolean add(TableRow tableRow);

    /**
     * Visualitza el contingut de l'estructura de dades.
     *
     * @param restrictions Restriccions per tal de filtrar files en la visualització.
     */
    protected abstract void select(TableRowRestriction restrictions);

    /**
     * Permet actualitzar una fila de l'estructura de dades.
     *
     * @param field El camp pel qual cercar la fila existent.
     * @param row   El contingut actualitzat de la fila.
     * @return true si s'ha actualitzat, false si no s'ha trobat el valor previ de la fila en l'estructura.
     */
    protected abstract boolean update(String field, TableRow row);

    /**
     * Si existeix el valor en l'estructura, en la columna especificada, llavors
     * elimina la primera coincidència de l'estructura. És a dir, la fila sencera.
     *
     * @param field El nom del camp o columna.
     * @param value El valor que ha de tenir el camp.
     * @return true si s'ha pogut eliminar la fila, false en cas contrari.
     */
    protected abstract boolean remove(String field, Object value);

    /**
     * @return El total d'elements que hi ha guardats en l'estructura.
     */
    protected abstract long size();

    public abstract TableRow getRow(Object indexValue);

    @Override
    public int importFromCSV (String path, Table table) {
        BufferedReader scanner = null;
        try {
            scanner = new BufferedReader(Files.newBufferedReader(Paths.get(path)));


            System.out.println("=========TIEMPO INICIO==========");
            long start = System.currentTimeMillis();
            long end;
            String ss = scanner.readLine();
            String[] columns = ss.trim().split(",");
            int count = 0;
            String s;
            while ((s = scanner.readLine()) != null) {
                String[] rowFields = s.split(",");
                TableRow row = new TableRow();
                for (int i = 0; i < columns.length; i++) {
                    String dataType = table.getColumnType(columns[i]).toString();
                    switch (dataType) {
                        case "TEXT":
                            row.setColumn(columns[i], rowFields[i]);
                            break;
                        case "INT":
                            row.setColumn(columns[i], Integer.valueOf(rowFields[i]));
                            break;
                        case "BOOLEAN":
                            row.setColumn(columns[i], Boolean.valueOf(rowFields[i]));
                            break;
                        case "LONG":
                            row.setColumn(columns[i], Long.valueOf(rowFields[i]));
                            break;
                        case "FLOAT":
                            row.setColumn(columns[i], Float.valueOf(rowFields[i]));
                            break;
                        case "DOUBLE":
                            row.setColumn(columns[i], Double.valueOf(rowFields[i]));
                            break;
                        case "CHAR":
                            row.setColumn(columns[i], rowFields[i].charAt(0));
                            break;
                    }
                }
                if (add(row)) count++;
            }
            end = System.currentTimeMillis();
            System.out.println("=========TIEMPO FINAL: " + String.valueOf(end - start) + "==========");
            return count;
        } catch (IOException e) {
            System.out.println("Error! No existeix el fitxer!");
            return 0;
        }

    }

    public abstract List<TableRow> getHistoric(Object indexValue);
}
